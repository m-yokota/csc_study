# 2018-10-27 社内勉強会
> ブロックチェーンをローカルで体感することを目的とする。
***
【前提】(Windows)  
- [VirtualBox](https://www.virtualbox.org/)  
- [Vagrant](https://www.vagrantup.com/)  
- [Git](https://git-scm.com/)  
	-　 gitをインストールしない場合は、https://bitbucket.org/m-yokota/csc_study より手動でダウンロードしても構築可能  
	- 　`git clone https://m-yokota@bitbucket.org/m-yokota/csc_study.git`  

```
csc_study  
　├ bitcoin  
　├ ethereum  
　├ hlf-marbles  
　└ hyperledger  
```

***
## 【１】bitcoin
- Vagrant立ち上げ
```
> cd bitcoin
bitcoin> vagrant up
bitcoin> vagrant ssh
```
- 環境確認
```
$ ip a | grep 192.168
    inet 192.168.33.18
$ cat /etc/os-release
    VERSION="16.04.5 LTS (Xenial Xerus)"
```
- インストール
```
$ mkdir /home/$(whoami)/src && cd /home/$(whoami)/src
$ git clone https://github.com/bitcoin/bitcoin.git
$ cd ./bitcoin
$ git tag   //(末尾にrcのついていないバージョンを選択)
$ git checkout -b 0.16.3 refs/tags/v0.16.3
$ ./autogen.sh
$ ./configure
$ make 
$ sudo make install
```
- 起動（デバッグモード）
```
$ bitcoind -regtest -printtoconsole &
 （デーモンとして起動する場合は 　bitcoind -regtest -daemon　）
```
- 終了
```
bitcoin-cli -regtest stop
```
- ブロックの生成
```
$ bitcoin-cli -regtest generate 101
```
>regtestモードでは、マイニングの代わりにgenerateコマンドを実行することにより、任意のタイミングでブロックを生成することができる。

- ブロック数の確認
```
$ bitcoin-cli -regtest getblockcount
```
- アカウントの生成
```
$ bitcoin-cli -regtest getnewaddress hoge1

    2NDamHiCjMMbRK83BxtmWMdFU2o78w2UVmC

$ bitcoin-cli -regtest getnewaddress hoge2

    2MvTtq7HapS2BzL5PLQCZUmcWLcwfLpyWk4
```
- 残高の確認
```
$ bitcoin-cli -regtest getbalance
$ bitcoin-cli -regtest getbalance hoge1
$ bitcoin-cli -regtest getbalance hoge2
$ bitcoin-cli -regtest listaccounts 0
```
- 送金（マイナーから`hoge1`へ）
```
$ bitcoin-cli -regtest sendtoaddress 2NDamHiCjMMbRK83BxtmWMdFU2o78w2UVmC 10
```
- トランザクションの確認
```
$ bitcoin-cli -regtest listunspent
$ bitcoin-cli -regtest gettransaction 8705dbc7fb1d609cc7c4aa35b33077368211288aabc611ddc0a9d0ef7219abe5
```
- マイニング実行
```
$ bitcoin-cli -regtest generate 1
```
> さらにブロックを追加で１つ作成し、
`bitcoin-cli -regtest gettransaction 8705dbc7fb1d609cc7c4aa35b33077368211288aabc611ddc0a9d0ef7219abe5`
で承認数をﾁｪｯｸ(confirmations)

- 送金（別アドレスへ）
```
$ bitcoin-cli -regtest getaccountaddress hoge1
$ bitcoin-cli -regtest sendtoaddress 2NCvc1h6v8XXAjHNcDjMxf1RVzhfp9mrSzE 5
```

- hoge1 ⇒ hoge2へ送金
```
$ bitcoin-cli -regtest sendfrom hoge1 2N4Ft9WyW6XLvg1E1YLdbG9hjyDsFSMAjZq 1
```

- 環境のリセット
```
$ rm -rf ~/.bitcoin/regtest
```
***
## 【２】Ethereum

- Vagrant立ち上げ
```
> cd ethereum
ethereum> vagrant up
ethereum> vagrant ssh
```
- 環境確認
```
$ ip a | grep 192.168
    inet 192.168.33.21
$ cat /etc/os-release
    VERSION="16.04.5 LTS (Xenial Xerus)"
```
### 環境構築
- Gethのインストール
```
$ sudo add-apt-repository -y ppa:ethereum/ethereum
$ sudo apt update -y
$ sudo apt install -y ethereum
```
- Gethのアップデート
```
$ sudo apt update -y
$ sudo apt upgrade -y
```
- プライベートネットワークへの接続
```
$ mkdir -p ~/eth_data/blockchaindata
$ cp /vagrant/myGenesis.json ~/eth_data/ && chmod -x ~/eth_data/myGenesis.json
$ geth --datadir=~/eth_data/blockchaindata/ init ~/eth_data/myGenesis.json
```
- gethの起動
```
$ geth --networkid "53967600" --nodiscover --datadir=~/eth_data/blockchaindata console 2>> ~/eth_data/node.log
```
- Genesisブロック情報の確認
```
> eth.getBlock(0)
```

### 採掘から送金まで
- アカウントの作成
```
> personal.newAccount("hoge1")
> personal.newAccount("hoge2")
> personal.newAccount("hoge3")

> eth.accounts
> eth.coinbase
```
- 採掘（マイニング）
```
> miner.start()

> eth.mining

> miner.stop()

> eth.blockNumber
```
- 送金
```
> eth.getBalance(eth.accounts[0])
> web3.fromWei(eth.getBalance(eth.accounts[0]),"ether")
> personal.unlockAccount(eth.accounts[0])
> eth.sendTransaction({from: eth.accounts[0], to: eth.accounts[1], value: web3.toWei(100, "ether")})
```
- 未確認トランザクション確認
```
> eth.pendingTransactions
```

### スマートコントラクト
- Solidityコンパイラのインストール
```
$ sudo add-apt-repository -y ppa:ethereum/ethereum
$ sudo apt-get update -y
$ sudo apt-get install -y solc
$ solc --version

$ cp /vagrant/test.sol ~/eth_data/ && chmod -x ~/eth_data/test.sol
$ solc --abi --bin ~/eth_data/test.sol
```
以下、gethにて操作
```
> var bin = "0x608060405234801561001057600080fd5b5060df8061001f6000396000f3006080604052600436106049576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806360fe47b114604e5780636d4ce63c146078575b600080fd5b348015605957600080fd5b5060766004803603810190808035906020019092919050505060a0565b005b348015608357600080fd5b50608a60aa565b6040518082815260200191505060405180910390f35b8060008190555050565b600080549050905600a165627a7a72305820741354c5a05a70256c06c7c131a1edf0d8c9dfcf246d1d4b8791c9a72c8b190d0029"
> var abi = [{"constant":false,"inputs":[{"name":"x","type":"uint256"}],"name":"set","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"get","outputs":[{"name":"retVal","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]

> var contract = eth.contract(abi)
> personal.unlockAccount(eth.accounts[0])
> var myContract = contract.new({from: eth.accounts[0], data: bin})
> var cnt = eth.contract(myContract.abi).at(myContract.address);
> cnt.set.sendTransaction(45, {from:eth.accounts[0]})

> cnt.get()
```

***Solidity の学習はここで***　→　[CryptoZombies](https://cryptozombies.io/jp/)

***
## 【３-1】Hyperledger(マルチ組織)
> かなりのマシンパワーおよびメモリを消費するので注意
- Vagrant立ち上げ
```
> cd csc_study
csc_study> vagrant up
```

## キーバリューサーバーでの操作
- 接続
```
csc_study> vagrant ssh hlf-kvserver
```
- 起動確認
```
$ docker network ls
```
`progrium/consul` が起動していればOK  
起動されていなかった場合、下記コマンドで起動させる。
```
$ docker run -d -p 8500:8500 -h consul progrium/consul -server -bootstrap
```

## メインマシンでの操作
- 接続
```
csc_study> vagrant ssh hlf-main
```
- オーバーレイネットワークの作成 & 確認
```
$ sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.100:8500 --cluster-advertise=192.168.33.111:2376/' /lib/systemd/system/docker.service -i
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
$ docker network create -d overlay overlay_byfn
$ docker network ls
```
- 起動
```
$ git clone https://m-yokota@bitbucket.org/m-yokota/csc_study.git
$ ~/csc_study/hyperledger/generate.sh
$ IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml -f docker-compose-couch.yaml up -d 2>&1
$ docker exec cli scripts/_script.sh
```
- 確認
```
$ docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Command}}\t{{.Ports}}"
```


## サブマシンでの操作
```
csc_study> vagrant ssh hlf-sub
```
- オーバーレイネットワークの作成 & 確認
```
$ sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.100:8500 --cluster-advertise=192.168.33.112:2376/' /lib/systemd/system/docker.service -i
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
$ docker network ls
```
- 起動
```
$ cd fabric-dev-servers/fabric-samples/first-network
$ ~/csc_study/hyperledger/sub/add.sh
```

## メインマシンでの操作
- 別ノードからのPeerの参加
```
$ docker exec -it cli bash

# CORE_PEER_LOCALMSPID="Org2MSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp CORE_PEER_ADDRESS=peer2.org2.example.com:7051 peer channel join -b mychannel.block 
# exit
```

- Restサーバーの立ち上げ
```
$ ~/csc_study/hyperledger/vagrant/rest.sh
$ export PEMPATH=/home/$(whoami)/csc_study/hyperledger/csc_study
$ composer network start -c PeerAdmin@byfn-network-org1 -n h29-03b-iothouse -V 0.0.1 -A alice -C $PEMPATH/alice/admin-pub.pem -A bob -C $PEMPATH/bob/admin-pub.pem
$ composer card create -p /tmp/composer/org1/byfn-network-org1.json -u alice -n h29-03b-iothouse -c $PEMPATH/alice/admin-pub.pem -k $PEMPATH/alice/admin-priv.pem
$ composer card import -f alice@h29-03b-iothouse.card
$ composer card create -p /tmp/composer/org2/byfn-network-org2.json -u bob -n h29-03b-iothouse -c $PEMPATH/bob/admin-pub.pem -k $PEMPATH/bob/admin-priv.pem
$ composer card import -f bob@h29-03b-iothouse.card
$ composer-rest-server -p 3000 -c bob@h29-03b-iothouse
```

下記アドレスにアクセス  
`http://192.168.33.111:3000`

***
## 【３-2】Hyperledger(marblesサンプル)
- Vagrant立ち上げ
```
> cd hlf-marbles
hlf-marbles> vagrant up
hlf-marbles> vagrant ssh
```
- 環境構築
```
$ mkdir /home/$(whoami)/fabric && cd /home/$(whoami)/fabric
$ curl -sSL https://goo.gl/kFFqh5 | bash -s 1.0.6
$ git clone https://github.com/hyperledger/fabric-samples.git -b v1.0.6
$ git clone https://github.com/IBM-Blockchain/marbles.git --single-branch --branch v4.0
$ cd /home/$(whoami)/fabric/marbles && git checkout a1f3723d
$ sed -e '/grpc/d' -e '/fabric-client/a \\t\t\"grpc\": \"1.10.1\",'  package.json -i
$ sudo npm install gulp -g
$ npm install
$ rm -rf ~/.hfc-key-store && mkdir ~/.hfc-key-store
$ cd ~/fabric/fabric-samples/basic-network/
$ ./start.sh
$ cd ~/fabric/marbles/scripts/
$ node install_chaincode.js
$ node instantiate_chaincode.js
```
- 起動
```
$ cd ~/fabric/marbles/
$ gulp marbles_local &
```
下記アドレスにアクセス  
`http://192.168.33.99:3001`

- couchDB  
`http://192.168.33.99:5984/_utils/`


- 停止
```
$ fg
^C
$ cd ../fabric-samples/basic-network/
$ ./stop.sh
$ docker rm $(docker ps -aq -f 'name=dev-*')
$ rm -rf ~/.hfc-key-store
$ ./teardown.sh
```

***
