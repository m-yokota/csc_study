#!/bin/bash

## docker exec -it cli bash
## docker cp ./scripts/_script.sh cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/_script.sh


export GITTPATH=/home/$(whoami)/csc_study/hyperledger
cp $GITTPATH/crypto-config.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITTPATH/docker-compose-e2e-template.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITTPATH/docker-compose-cas-template.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/

cp $GITTPATH/docker-compose-cli.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITTPATH/docker-compose-couch.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITTPATH/configtx.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITTPATH/docker-compose-base.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/base/
cp $GITTPATH/peer-base.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/base/
cp $GITTPATH/_script.sh /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/scripts/
chmod +x /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/scripts/_script.sh

export PATH=${PWD}/../bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}
rm -rf crypto-config && cryptogen generate --config=./crypto-config.yaml
cp docker-compose-e2e-template.yaml docker-compose-e2e.yaml
cp docker-compose-cas-template.yaml docker-compose-cas.yaml

pushd crypto-config/peerOrganizations/org1.example.com/ca/ && PRIV_KEY=$(ls *_sk) && popd
sed -i "s/CA1_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-e2e.yaml docker-compose-cas.yaml
pushd crypto-config/peerOrganizations/org2.example.com/ca/ && PRIV_KEY=$(ls *_sk) && popd
sed -i "s/CA2_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-e2e.yaml docker-compose-cas.yaml

CHANNEL_NAME="mychannel"
configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

export VAGRANTPATH=/vagrant
rm -rf $VAGRANTPATH/crypto-config
cp -r crypto-config $VAGRANTPATH/

echo "Please execute the following command"
echo "→ IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml -f docker-compose-couch.yaml up -d 2>&1"
echo "→ docker exec cli scripts/_script.sh"


#docker rmi `docker images | sed -ne '2,$p' -e 's/  */ /g' | awk '{print $1":"$2}'`
#docker ps -aq | xargs docker stop | xargs docker rm

## CORE_PEER_LOCALMSPID="Org2MSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp CORE_PEER_ADDRESS=peer2.org2.example.com:7051 peer channel join -b mychannel.block
