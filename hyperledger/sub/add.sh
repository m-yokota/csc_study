#!/bin/bash

# docker ps -aq | xargs docker rm

export GITPATH=/home/$(whoami)/csc_study/hyperledger
cp $GITPATH/sub/docker-compose-base.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/base/
cp $GITPATH/sub/docker-compose-cli.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/
cp $GITPATH/sub/docker-compose-couch.yaml /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/

export VAGRANTPATH=/vagrant
sudo rm -rf /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/crypto-config
cp -r $VAGRANTPATH/crypto-config /home/$(whoami)/fabric-dev-servers/fabric-samples/first-network/

#CHANNEL_NAME="mychannel" TIMEOUT=10000 DELAY=3 LANG=golang docker-compose -f docker-compose-cli.yaml up -d 2>&1
#CHANNEL_NAME="mychannel" TIMEOUT=10000 DELAY=3 LANG=golang docker-compose -f docker-compose-cli.yaml -f docker-compose-couch.yaml up -d 2>&1

###IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml -f docker-compose-couch.yaml up -d 2>&1
IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-couch.yaml up -d 2>&1
