/* global getAssetRegistry */

/**
 * Trade a marble to a new player
 * @param  {org.yokotam.cscnet.marbles.TradeMarble} tradeMarble - the trade marble transaction
 * @transaction
 */
async function tradeMarble(tradeMarble) {   // eslint-disable-line no-unused-vars
    tradeMarble.marble.owner = tradeMarble.newOwner;
    const assetRegistry = await getAssetRegistry('org.yokotam.cscnet.marbles.Marble');
    await assetRegistry.update(tradeMarble.marble);
}
